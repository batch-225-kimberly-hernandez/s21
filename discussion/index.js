// [SECTION] Array

// An array in programming is simply a list of data.
// They are declared using square bracker ([]) also known as "Array Literals"
// Commonly used to store numerous amounts of data to manipulate in order to perform a number of tasks.

/* 

*/

let studentNumberA = '2020-1923'
let studentNumberB = '2020-1924'
let studentNumberC = '2020-1925'
let studentNumberD = '2020-1926'
let studentNumberE = '2020-1927'

let studentNumbers = ['2020-1923', '2020-1924', '2020-1925', '2020-1926', '2020-1927']

console.log(studentNumbers)


// Common examples of arrays

let grades = [98.5, 94.3, 89.2, 90.1]
let computerBrands = ['Acer', 'Asus', 'Lenovo', 'Redfox', 'Gateway', 'Toshiba', 'Fujitsu']

// Possible use of an array but some of this is not recommended

let mixedArr = [12, 'Asus', null, undefined, {}]

console.log(grades)
console.log(computerBrands)
console.log(mixedArr)


// Alternative way to write arrays
let myTasks = [
    'drink html',
    'eat JavaScript',
    'inhale css',
    'bake css'
]

// 

let city1 = "Tokyo"
let city2 = "Manila"
let city3 = "Jakarta"

let cities = [city1, city2, city3]

console.log(myTasks)
console.log(cities)

// [SECTION] .length property

// The .length property allows us to get and set the total numbers of items in an array.

console.log(myTasks.length)
console.log(cities.length)

let blankArr = []
console.log(blankArr.length)

myTasks.length = myTasks.length - 2
console.log(myTasks.length)
console.log(myTasks)

cities.length--;
console.log(cities)

let theBeatles = ['John', 'Paul', 'Ringo', 'George']
theBeatles.length++
console.log(theBeatles)

// [SECTION] Reading from Arrays

// Accessing array elements is one of the more common tasks that we do with an aray. This can be done through the use of array indexes.

/* 
    Syntax:
        arrayName[index]
*/

console.log(grades[0])
console.log(computerBrands[3])
console.log(grades[20])

let lakersLegends = ['Kobe', 'Shaq', 'Lebron', 'Magic', 'Kareem']

console.log('Array before reassignment')
console.log(lakersLegends)
lakersLegends[2] = "Pau Gasol"

console.log('Array after reassignment')
console.log(lakersLegends)